Name:           mib
Version:        1.9.6
Release:        1.0%{?dist}
Summary:        An MPI-coordinated test that performs operations on files and directories
License:        GPL-2.0
Group:          System/Benchmark
Url:            http://mdtest.sourceforge.net/
Source:         http://sourceforge.net/projects/mdtest/files/mdtest%%20latest/%{name}-%{version}/%{name}-%{version}.tar.gz
BuildRequires:  mpich-devel
Requires: mpich
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
mdtest is an MPI-coordinated metadata benchmark test that performs
open/stat/close operations on files and directories and then reports the
performance.

%prep
%setup -q


%build
%{?_mpich_load}
%configure
make %{?_smp_mflags}  MPI_CC="mpicc"
%{?_mpich_unload}

%install
%{?_mpich_load}
%if 0%{?rhel} < 8
MPI_BIN=%{_bindir}
%endif

#make install
install -D -p -m 0755 mib   %{buildroot}${MPI_BIN}/mib
#install -D -p -m 0644 mdtest.1 %{buildroot}%{_mandir}/man1/mdtest.1
%{?_mpich_unload}

%files
%defattr(-,root,root)

%if 0%{?rhel} >= 8
%{_libdir}/mpich/bin/mib
%else
%{_bindir}/mib
%endif

%changelog
* Sun Aug 5 2018 c17454@cray.com
- use mpich instead of openmpi
