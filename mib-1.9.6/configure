#!/bin/bash
#*****************************************************************************\
#*  $Id: configure 1.9 2005/11/30 20:24:57 auselton Exp $
#*****************************************************************************
#*  Copyright (C) 200 The Regents of the University of California.
#*  Produced at Lawrence Livermore National Laboratory (cf, DISCLAIMER).
#*  Written by Andrew Uselton <uselton2@llnl.gov>
#*  UCRL-CODE-222725
#*  
#*  This file is part of Mib, an MPI-based parallel I/O benchamrk
#*  For details, see <http://www.llnl.gov/linux/mib/>.
#*  
#*  Mib is free software; you can redistribute it and/or modify it under
#*  the terms of the GNU General Public License (as published by the Free
#*  Software Foundation) version 2, dated June 1991.
#*  
#*  Mib is distributed in the hope that it will be useful, but WITHOUT 
#*  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
#*  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
#*  for more details.
#*  
#*  You should have received a copy of the GNU General Public License along
#*  with Mib; if not, write to the Free Software Foundation, Inc.,
#*  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
#*****************************************************************************/
# configure
# This is not an autoconf generated file.  It's just an intermediary hack to
# notice if I need to use the BGL cross compilers.  Once I sort out real 
# autoconf stuff we'll get rid of this.

PROG=$(basename $0)

#NODEATTR=`/usr/bin/which nodeattr 2>/dev/null`
#[ X"$NODEATTR" == X ] && { echo "This script expects a cluster with the Genders \"nodeattr\" function installed"; exit 1; } 
#CLUSTER=`$NODEATTR -v cluster`

if [ X"$ARCH" == X ]
    then
    ARCH=`uname -m`
    [ X"$ARCH" == X ] && usage "Failed to detect architecture"
    [ "$ARCH" == "i686" -o "$ARCH" == "i586" -o "$ARCH" == "i486" ] && ARCH="i386"
fi

usage ()
{
	[ X"$1" == X ] || { echo "$1"; echo; } 
	echo "$PROG"
	echo "Set up for a compile if possible"
	echo "Print an error message otherwise"
	echo "This script establishes the compiler to use in the makefile based on the"
	echo "cluster as returned by \"nodeattr -v cluster\", and sets the version,"
	echo "based on the value of VERSION in the \"META\" file, in the source at"
	echo "\"mib.c\" and the spec file \"mib.spec\"."
	exit 1
}

[ X"$1" != X"-h" ] || usage
CC="mpicc"
LIBS="-ldl"
INTERCONNECT_LIBRARY_HEADER="elan.h"
case $ARCH in
	i386) 
	  ;;
	bgl) 
	CC="mpicc"
	LIBS=""
# Truely unfortunate hack to get BGL to work at all.  There is no dlfcn.h 
# for the mpgcc cross compiler.
	cp bgl_mpi_wrap.c mpi_wrap.c
	  ;;
	x86_64) 
	INTERCONNECT_LIBRARY_HEADER="infiniband.h"
	  ;;
	ia64)
	  ;;
	*) usage "This script needs to know what compiler to use for architecture $ARCH"
	  ;;
esac 
echo "Cluster $CLUSTER (architecture $ARCH) with compiler $CC"

MAKEFILE="Makefile"
[ -f $MAKEFILE.in ] || usage "There is no file $MAKEFILE.in here"
CONFIG_H="config.h"
MIB_SPEC="mib.spec"
[ -f $MIB_SPEC.in ] || usage "There is no file $MIB_SPEC.in here"
[ -f META ] || usage "There is no file META here"

VERSION=`grep VERSION META | cut -d':' -f2`
VERSION=${VERSION// /}
[ X"$VERSION" != X ] ||  usage "The META file did not yield a version"

rm -f $CONFIG_H >/dev/null 2>&1
cp doc/COPYRIGHT.block $CONFIG_H
echo "#ifndef CONFIG_H" >>$CONFIG_H
echo "#define MIB_VERSION \"$VERSION\"" >> $CONFIG_H
echo "#define MIB_ARCH \"$ARCH\"" >> $CONFIG_H
MPI_H="/usr/lib/mpi/include/mpi.h"
MPI_DIR="/usr/lib/mpi"
if [ -f $MPI_H -o -d $MPI_DIR ]
then
    echo "#include \"$INTERCONNECT_LIBRARY_HEADER\"" >> $CONFIG_H
else
    echo "#define MPI_NOT_INSTALLED" >> $CONFIG_H
fi
echo "#endif /* CONFIG_H */" >> $CONFIG_H

rm -f $MAKEFILE >/dev/null 2>&1
cat $MAKEFILE.in  | sed "s/%CC%/$CC/" | sed "s/%LIBS%/$LIBS/" | sed "s/%VERSION%/$VERSION/" > $MAKEFILE

cd tools
rm -f $MAKEFILE >/dev/null 2>&1
cat $MAKEFILE.in  | sed "s/%CC%/$CC/" | sed "s/%VERSION%/$VERSION/" > $MAKEFILE
cd ..

# Suppress making the pdf documentation if the pdflatex executable is 
# not available.  Later I could split this out to just latex without dvi2pdf.
PDFLATEX=`which pdflatex`
if [ X"$PDFLATEX" == X ] 
then
    touch doc_dummy 
    touch doc/mib.pdf
    touch doc/sample.png
fi
cd doc
rm -f $MAKEFILE >/dev/null 2>&1
cat $MAKEFILE.in  | sed "s/%VERSION%/$VERSION/" > $MAKEFILE
cd ..

rm -f $MIB_SPEC >/dev/null 2>&1
cat $MIB_SPEC.in  | sed "s/%VERSION%/$VERSION/" > $MIB_SPEC


 
