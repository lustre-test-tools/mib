TOPDIR=/tmp/rpmbuild
rm -rf $TOPDIR
rm -f *.rpm
mkdir -p $TOPDIR/SOURCES

tar czf $TOPDIR/SOURCES/mib-1.9.6.tar.gz mib-1.9.6
rpmbuild --define "_topdir $TOPDIR" -bs mib.spec

cp $TOPDIR/SRPMS/*.src.rpm .
rpmbuild --define "_topdir $TOPDIR" --define "_bindir /usr/local/bin/" --rebuild *.src.rpm
cp $TOPDIR/RPMS/*/* .